CREATE TABLE user_style(
id int(11) NOT NULL AUTO_INCREMENT,
height int(11) not null,
weight int(11) not null,
first_weight int(11) not null,
sex varchar(11) not null,
bmi double not null,
user_id int(11) not null,
bmi_id int(11) not null,
  PRIMARY KEY (id)
);
CREATE TABLE calorie(
id int(11) NOT NULL AUTO_INCREMENT,
breakfast_calorie int(11) not null,
lunch_calorie int(11) not null,
dinner_calorie int(11) not null,
total_calorie int(11) not null,
calorie_style varchar(11) not null,
calorie_date date,
user_style int(11),
  PRIMARY KEY (id)
);
,,
CREATE TABLE diet_menu(
menu_id int(11) NOT NULL AUTO_INCREMENT,
bmi_id int(11) NOT NULL,
workout_menu varchar(255) not null,
eat_menu varchar(255) not null,
style_explanation varchar(255) not null,
workout_style varchar(255) not null,
recommended_workout varchar(255) not null,
recommended_eat_menu varchar(255) not null,
 PRIMARY KEY (menu_id)
 );
CREATE TABLE bmi(
id int(11) NOT NULL AUTO_INCREMENT,
style varchar(255) not null,
 PRIMARY KEY (id)
 );
CREATE TABLE user (
  `d int(11) NOT NULL AUTO_INCREMENT,
  user_id varchar(11) NOT NULL,
  password varchar(256) NOT NULL,
  create_date` date NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE workout (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_style_id int(11) NOT NULL,
  start_time time NOT NULL,
  finish_time time NOT NULL,
  work_time time NOT NULL,
  create_date date NOT NULL,
  PRIMARY KEY (id)
);
ALTER TABLE user_style
ADD old VARCHAR(20) NULL;

INSERT INTO bmi(style)
VALUES('?せすぎ');
INSERT INTO bmi(style)
VALUES('?せぎみ');
INSERT INTO bmi(style)
VALUES('普通体重');
INSERT INTO bmi(style)
VALUES('太り気味');
INSERT INTO bmi(style)
VALUES('肥満');
INSERT INTO bmi(style)
VALUES('肥満(すぎ)');
CREATE TABLE clorie_renge (
  id int(11) NOT NULL AUTO_INCREMENT,
  old_renge int(11) NOT NULL,
  men varchar(255) not null,
  women varchar(255) not null,
  PRIMARY KEY (id)
);
ALTER TABLE user_style
DROP COLUMN sex＝-;

ALTER TABLE clorie_renge
ADD women int(11) NOT NULL;
INSERT INTO  clorie_renge(old_renge,men,women)
VALUES('12〜14(歳)',2600,2400);
INSERT INTO  clorie_renge(old_renge,men,women)
VALUES('15〜17(歳)',2850,2300);
INSERT INTO  clorie_renge(old_renge,men,women)
VALUES('18〜29(歳)',2650,1950);
INSERT INTO  clorie_renge(old_renge,men,women)
VALUES('30〜49(歳)',2650,2000);
INSERT INTO  clorie_renge(old_renge,men,women)
VALUES('50〜69(歳)',2450,1900);
INSERT INTO  clorie_renge(old_renge,men,women)
VALUES('70〜(歳)',2200,1750);

ALTER TABLE user_style
ADD renge_id int(11) NOT NULL;

INSERT INTO  clorie_renge(old_renge,men,women)
VALUES('70〜(歳)',2200,1750);

DELETE FROM calorie;
INSERT INTO  clorie_renge(id,user_id,password,create_date)
VALUES('7','user_id,pass,2020/10/13);
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) NOT NULL,
  `password` varchar(256) NOT NULL,
  `create_date` date NOT NULL,
ALTER TABLE clorie_renge
 RENAME COLUMN men TO 1;
 ALTER TABLE clorie_renge
  CHANGE COLUMN women `2` int(11);
  
  CREATE TABLE sample(
id int(11) NOT NULL AUTO_INCREMENT,
calorie_date date,
  PRIMARY KEY (id)
);
ALTER TABLE sample
DROP COLUMN calorie_date;
ALTER TABLE  workout
ADD workouttime time NOT NULL;
ALTER TABLE workout
DROP COLUMN start_time;
ALTER TABLE workout
DROP COLUMN finish_time;
ALTER TABLE workout
DROP COLUMN work_time;
ALTER TABLE workout
ADD workouttime int(11) NOT NULL;
DROP TABLE menu;


ALTER TABLE menu
ADD recommended_eat_menu varchar(256) NOT NULL;

INSERT INTO  diet_menu(bmi_id,workout_menu,eat_menu,style_explanation,workout_style,recommended_workout,recommended_eat_menu)
VALUES('1','有酸素運動','3食しっかりと、決まった時間に食べる。','体重も体脂肪も少ない「やせすぎ」タイプ、一番死亡率が高く体重を落とさないよう食事をしっかりと取り、バランスのよいカラダを目指します。','有酸素運動で体調を整える。','ウォーキング。ジョギング','脂肪は控えて、エネルギー比率を上げましょう！主食(白米・パン)5, 主菜3, 副菜2');

INSERT INTO diet_menu(bmi_id,workout_menu,eat_menu,style_explanation,workout_style,recommended_workout,recommended_eat_menu)
VALUES('2','有酸素運動','3食しっかりと、決まった時間に食べる。','体重も体脂肪も少ない「やせ型」タイプ、体重を落とさないよう食事をしっかりと取り、バランスのよいカラダを目指します。','有酸素運動で体調を整える。','ウォーキング。ジョギング','脂肪は控えて、エネルギー比率を上げましょう！主食(白米・パン)5, 主菜3, 副菜2');

INSERT INTO  diet_menu(bmi_id,workout_menu,eat_menu,style_explanation,workout_style,recommended_workout,recommended_eat_menu)
VALUES('3','有酸素運動','3食しっかりと、決まった時間に食べる。','体重も体脂肪も少ない「やせぎみ」タイプ、体重を落とさないよう食事をしっかりと取り、バランスのよいカラダを目指します。','有酸素運動で体調を整える。','ウォーキング。ジョギング','脂肪は控えて、エネルギー比率を上げましょう！主食(白米・パン)5, 主菜3, 副菜2');

INSERT INTO  diet_menu(bmi_id,workout_menu,eat_menu,style_explanation,workout_style,recommended_workout,recommended_eat_menu)
VALUES('4','有酸素運動,無酸素運動','3食しっかりと、決まった時間に食べる。','体脂肪、体重ともに標準の「普通」タイプ場合は、この数値を維持しつつ、カラダを引き締めるようにします。','腹筋運動やスクワットなど、引き締めたい部分の筋力トレーニングを行う。','ジョギング、スクワット、プランク','高タンパク質、低カロリー（鶏胸肉、鶏のささ身、納豆、ツナ）');

INSERT INTO  diet_menu(bmi_id,workout_menu,eat_menu,style_explanation,workout_style,recommended_workout,recommended_eat_menu)
VALUES('5','有酸素運動,無酸素運動','3食のうち1食を、ローカロリー食に置き換える。','体脂肪率、BMI共にやや高めの、「太りぎみ」タイプです。食事と運動の両面から、バランスよくカラダを整えていきます。','ウォーキングなどの有酸素運動を20分と、日々の筋力トレーニングをそれぞれ行う。','ジョギング、スクワット、スイミング','高タンパク質、低カロリー（鶏胸肉、鶏のささ身、納豆、ツナ）');

INSERT INTO  diet_menu(bmi_id,workout_menu,eat_menu,style_explanation,workout_style,recommended_workout,recommended_eat_menu)
VALUES('6','有酸素運動','3食のうち1〜2食を、ローカロリー食に置き換え、一日の総摂取カロリーをコントロールする。','体脂肪、BMI共に高い「肥満」タイプです。食事制限と運動療法の両面からダイエットを行いましょう。また生活習慣病の原因ともなります.','ウォーキングなどの有酸素運動を20分と、日々の筋力トレーニングをそれぞれ行う。また、日常生活でも運動習慣を身につける。','ウオーキング、スイミング','高タンパク質、低カロリー、最初は腹持ちのいいもの(玄米、キノコ、鶏胸肉、鶏のささ身、納豆、ツナ)');

INSERT INTO  diet_menu(bmi_id,workout_menu,eat_menu,style_explanation,workout_style,recommended_workout,recommended_eat_menu)
VALUES('7','有酸素運動','3食のうち1〜2食を、ローカロリー食に置き換え、一日の総摂取カロリーをコントロールする。','体脂肪、BMI共に高い「肥満」タイプです。食事制限と運動療法の両面からダイエットを行いましょう。また生活習慣病の原因ともなりますので、一度医師の診察を受けることをオススメします。','ウォーキングなどの有酸素運動を20分と、日々の筋力トレーニングをそれぞれ行う。また、日常生活でも運動習慣を身につける。','ウオーキング、スイミング','高タンパク質、低カロリー、最初は腹持ちのいいもの(玄米、キノコ、鶏胸肉、鶏のささ身、納豆、ツナ)');

drop table menu;

 SELECT * FROM user_style JOIN diet_menu ON user_style.bmi_id=diet_menu.bmi_id JOIN bmi ON user_style.bmi_id=bmi.id ;

alter table user_style change  staffid bigint unique;
SELECT *,SUM(workouttime) FROM user_style JOIN workout ON user_style.id = workout.user_style_id JOIN calorie ON user_style.id = calorie.user_style;

DELETE FROM calorie WHERE id="6";
select sec_to_time(sum( time_to_sec(workouttime))) as total_time from workout;
