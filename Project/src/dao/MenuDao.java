package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;
import beans.MenuBeans;

public class MenuDao {

	public MenuBeans findmenu(int id) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user_style JOIN diet_menu ON user_style.bmi_id=diet_menu.bmi_id JOIN bmi ON user_style.bmi_id=bmi.id WHERE user_style.id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			String workout_menu = rs.getString("diet_menu.workout_menu");
			String eat_menu = rs.getString("diet_menu.eat_menu");
			String bmi_style = rs.getString("bmi.style");
			String style_explanation = rs.getString("diet_menu.style_explanation");
			String workout_style = rs.getString("diet_menu.workout_style");
			String recommended_workout = rs.getString("diet_menu.recommended_workout");
			String recommended_eat_menu = rs.getString("diet_menu.recommended_eat_menu");
			return new MenuBeans(workout_menu, eat_menu, style_explanation,
					workout_style, recommended_workout, recommended_eat_menu, bmi_style);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}
}
