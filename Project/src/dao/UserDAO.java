package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserBeans;

public class UserDAO {

	public UserBeans findByLoginInfo(String loginId, String password) throws SQLException {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE user_id = ? and password = ?";
			String result = pass(password);

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, result);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int id = rs.getInt("id");
			String userid = rs.getString("user_id");
			String password1 = rs.getString("password");

			return new UserBeans(id, userid, password1);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

	public void newUserId(String userid, String password) throws SQLException {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();
			String sql = "INSERT INTO  user(user_id,password,create_date)VALUES(?,?,now())";
			String result = pass(password);
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userid);
			pStmt.setString(2, result);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

		}
	}

	public void UserStyle(int sex, int old, int weight, int height) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO  user_style(old,height,weight,first_weight,sex,bmi,bmi_id,renge_id)VALUES(?,?,?,?,?,?,?,?)";
			double bmi = bmi(weight, height);
			int bmibody = bmiId(weight, height);
			int oldrenge = oldrenge(old);
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, old);
			pStmt.setInt(2, height);
			pStmt.setInt(3, weight);
			pStmt.setInt(4, weight);
			pStmt.setInt(5, sex);
			pStmt.setDouble(6, bmi);
			pStmt.setInt(7, bmibody);
			pStmt.setInt(8, oldrenge);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

		}

	}

	public UserBeans alreadyById(String userid) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE user_id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = (PreparedStatement) conn.prepareStatement(sql);
			pStmt.setString(1, userid);
			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return null;
			}
			int id = rs.getInt("id");
			String loginid = rs.getString("user_id");
			String password = rs.getString("password");
			return new UserBeans(id, loginid, password);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

	}

	private double bmi(int weight, int height) {
		double bmiheight = height / 100.00;
		return weight / bmiheight / bmiheight;
	}

	private int bmiId(int weight, int height) {
		double bmiheight = height / 100.00;
		double body = weight / bmiheight / bmiheight;
		if (body < 16) {
			int style = 1;
			return style;
		}
		if (body < 17) {
			int style = 2;
			return style;
		}
		if (body < 18.5) {
			int style = 3;
			return style;
		}
		if (body < 25) {
			int style = 4;
			return style;
		}
		if (body < 30) {
			int style = 5;
			return style;
		}
		if (body < 35) {
			int style = 6;
			return style;
		} else {
			int style = 7;
			return style;

		}

	}

	private int oldrenge(int old) {
		if (old < 14) {
			int style = 1;
			return style;
		}
		if (old < 17) {
			int style = 2;
			return style;
		}
		if (old < 29) {
			int style = 3;
			return style;
		}
		if (old < 49) {
			int style = 4;
			return style;
		}
		if (old < 69) {
			int style = 5;
			return style;
		} else {
			int style = 6;
			return style;

		}

	}

	public String pass(String password) {
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);
			return result;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return algorithm;

	}

}
