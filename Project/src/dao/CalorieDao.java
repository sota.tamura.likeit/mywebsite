package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;
import beans.EffortBeans;
import beans.UserBeans;


public class CalorieDao {
	public UserBeans findcalorieuser(int id) throws SQLException {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user_style WHERE id= ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int id1 = rs.getInt("id");
			int sex = rs.getInt("sex");
			int rengeid = rs.getInt("renge_id");

			return new UserBeans(id1, sex, rengeid);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}
	public UserBeans caloriealready(int id) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM calorie WHERE user_style = ? AND  calorie_date = date(now())";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int id1 = rs.getInt("id");

			return new UserBeans(id1);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}
	public EffortBeans newcalorieStyle(int id, int breakfast, int lunch, int dinner,  int sex, int oldrenge) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO  calorie(breakfast_calorie,lunch_calorie,dinner_calorie,total_calorie,calorie_style,calorie_date,user_style)VALUES(?,?,?,?,?,now(),?)";
			int totalCalorie = tatal(breakfast, lunch, dinner);
			int usually = usually(sex, oldrenge);
			String eatstyle = eatstyle(usually, totalCalorie);
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, breakfast);
			pStmt.setInt(2, lunch);
			pStmt.setInt(3, dinner);
			pStmt.setInt(4, totalCalorie);
			pStmt.setString(5, eatstyle);
			pStmt.setInt(6, id);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
		}
		return null;

	}
	public EffortBeans calorieStyle(int id, int breakfast, int lunch, int dinner,  int sex, int oldrenge) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "UPDATE calorie SET breakfast_calorie=?,lunch_calorie=?,dinner_calorie=?,total_calorie=?,calorie_style=? WHERE user_style=? AND calorie_date=date(now())";
			int totalCalorie = tatal(breakfast, lunch, dinner);
			int usually = usually(sex, oldrenge);
			String eatstyle = eatstyle(usually, totalCalorie);
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, breakfast);
			pStmt.setInt(2, lunch);
			pStmt.setInt(3, dinner);
			pStmt.setInt(4, totalCalorie);
			pStmt.setString(5, eatstyle);
			pStmt.setInt(6, id);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
		}
		return null;

	}

	private int tatal(int breakfast, int lunch, int dinner) {
		return breakfast + lunch + dinner;
	}

	private int usually(int sex, int oldrenge) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM clorie_renge WHERE id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, oldrenge);
			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return (Integer) null;
			}
			if(sex==1) {
				int calorie = rs.getInt("1");
				return calorie;
			}else {
				int calorie = rs.getInt("2");
				return calorie;
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

	private String eatstyle(int usually, int totalCalorie) {
		if (usually-400 >= totalCalorie ) {
			String style = "足りない";
			return style;
		}
		if (usually-200 >= totalCalorie) {
			String style = "少し食べてない";
			return style;
		}
		if (usually +200 >= totalCalorie) {
			String style = "適量";
			return style;
		}
		if (usually +400 >= totalCalorie ) {
			String style = "少し食べ過ぎ";
			return style;
		}if (usually +400 < totalCalorie ) {
			String style = "食べ過ぎ";
			return style;
		} else {
			return null;
		}

	}


}
