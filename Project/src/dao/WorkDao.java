package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;

import base.DBManager;

public class WorkDao {

	public void work(int id, Time time) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO  workout(user_style_id,workouttime,create_date)VALUES(?,?,now())";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			pStmt.setTime(2, time);
			pStmt.executeUpdate();
		} catch (SQLException e) {
            e.printStackTrace();
        } finally {

        }

    }

}
