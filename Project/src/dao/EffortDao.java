package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;

import base.DBManager;
import beans.EffortBeans;

public class EffortDao {
	public EffortBeans findeffort(int id) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * ,sec_to_time(sum( time_to_sec(workouttime))) AS total_workouttime FROM user_style JOIN workout ON user_style.id = workout.user_style_id JOIN calorie ON user_style.id=calorie.user_style JOIN bmi ON user_style.bmi_id=bmi.id WHERE user_style.id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int weight = rs.getInt("user_style.weight");
			int first_weigh = rs.getInt("user_style.first_weight");
			Time workouttime = rs.getTime("total_workouttime");
			String calorie_style = rs.getString("calorie.calorie_style");
			Double bmi = rs.getDouble("user_style.bmi");
			String style = rs.getString("bmi.style");
			return new EffortBeans(id, weight,bmi, first_weigh, style, calorie_style, workouttime);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

	public ArrayList<EffortBeans> findclorie(int id) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user_style JOIN calorie ON user_style.id=calorie.user_style JOIN clorie_renge ON user_style.renge_id = clorie_renge.id WHERE user_style.id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			int sex = sex(id);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();
			ArrayList<EffortBeans> itemList = new ArrayList<EffortBeans>();

			while (rs.next()) {
				EffortBeans effort = new EffortBeans();
				effort.setBreakfast(rs.getInt("breakfast_calorie"));
				effort.setLunch(rs.getInt("lunch_calorie"));
				effort.setDinner(rs.getInt("dinner_calorie"));
				effort.setTotal(rs.getInt("total_calorie"));
				effort.setCreatedate(rs.getDate("calorie_date"));
				effort.setCalorie_style(rs.getString("calorie_style"));
				if (sex == 1) {
					effort.setStanderd(rs.getInt("1"));

				} else {
					effort.setStanderd(rs.getInt("2"));
				}
				itemList.add(effort);
			}
			return itemList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

	private int sex(int id) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user_style WHERE id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return (Integer) null;
			}

			int sex = rs.getInt("sex");
			return sex;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

	public void changeweight(int weight, int id) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "UPDATE user_style SET weight = ?,bmi_id=?,bmi=? WHERE id=?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			int height= height(weight, id);
			int bmiId = bmiid(weight, height);
			double bmi = bmi(weight, height);
			pStmt.setInt(1, weight);
			pStmt.setInt(2, bmiId);
			pStmt.setDouble(3, bmi);
			pStmt.setInt(4, id);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

		}
	}

	private int height(int weight, int id) throws SQLException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user_style WHERE id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();
			if (!rs.next()) {
				return (Integer) null;
			}
			int height = rs.getInt("height");
			return height;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}
	private double bmi(int weight, int height) {
		double bmiheight = height / 100.00;
		double body = weight / bmiheight / bmiheight;
		return body;
	}

	private int bmiid(int weight, int height) {
		double bmiheight = height / 100.00;
		double body = weight / bmiheight / bmiheight;
		if (body < 16) {
			int style = 1;
			return style;
		}
		if (body < 17) {
			int style = 2;
			return style;
		}
		if (body < 18.5) {
			int style = 3;
			return style;
		}
		if (body < 25) {
			int style = 4;
			return style;
		}
		if (body < 30) {
			int style = 5;
			return style;
		}
		if (body < 35) {
			int style = 6;
			return style;
		} else {
			int style = 7;
			return style;

		}

	}

}
