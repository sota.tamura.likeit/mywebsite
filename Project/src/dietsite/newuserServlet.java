package dietsite;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBeans;
import dao.UserDAO;

/**
 * Servlet implementation class newuserServlet
 */
@WebServlet("/newuserServlet")
public class newuserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		request.getRequestDispatcher(EcHelper.NEWUSER_PAGE).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String userid = request.getParameter("userid");
		String pass = request.getParameter("pass");
		String repass = request.getParameter("repass");
		int sex = Integer.valueOf(request.getParameter("sex")).intValue();
		int old = Integer.valueOf(request.getParameter("old")).intValue();
		int weight = Integer.valueOf(request.getParameter("weight")).intValue();
		int height = Integer.valueOf(request.getParameter("height")).intValue();
		UserDAO UserDao = new UserDAO();
		UserBeans already = UserDao.alreadyById(userid);
		if (!(pass.equals(repass))) {
			request.setAttribute("errMsg", "パスワードとパスワード（確認）が異なります");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newuser.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (already == null) {
			try {
				UserDao.newUserId(userid, pass);
				UserDao.UserStyle(sex, old, weight, height);
				request.getRequestDispatcher(EcHelper.LOGIN_PAGE).forward(request, response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			request.setAttribute("errMsg", "すでに存在しています。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newuser.jsp");
			dispatcher.forward(request, response);
			return;
		}
	}
}
