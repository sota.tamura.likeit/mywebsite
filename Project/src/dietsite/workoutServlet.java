package dietsite;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Time;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.WorkDao;

@WebServlet("/workoutServlet")
public class workoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("indexServlet");
			return;
		}
		request.setCharacterEncoding("UTF-8");
		int id = Integer.valueOf(request.getParameter("id")).intValue();
		request.setAttribute("workout", id);
		request.getRequestDispatcher(EcHelper.WORKOUT_PAGE).forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		WorkDao work = new WorkDao();
		int id = Integer.valueOf(request.getParameter("id")).intValue();
		Time time = Time.valueOf(request.getParameter("time"));

		if (id == 0) {
			request.setAttribute("errMsg", "入力されていません。");
			request.getRequestDispatcher("/WEB-INF/jsp/workout.jsp");
			return;
		} else {
			try {
				work.work(id, time);
				request.getRequestDispatcher(EcHelper.MEIN_PAGE).forward(request, response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
