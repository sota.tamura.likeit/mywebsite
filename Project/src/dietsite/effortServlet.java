package dietsite;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.EffortBeans;
import dao.EffortDao;

/**
 * Servlet implementation class effortServlet
 */
@WebServlet("/effortServlet")
public class effortServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("indexServlet");
			return;
		}
		int id = Integer.valueOf(request.getParameter("id")).intValue();
		try {
			EffortDao effortDao = new EffortDao();
			EffortBeans effort = effortDao.findeffort(id);
			request.setAttribute("effort", effort);
			HttpSession page = request.getSession();
			ArrayList<EffortBeans> calorie = effortDao.findclorie(id);
			page.setAttribute("calorie", calorie);
			request.getRequestDispatcher(EcHelper.EFFORT_PAGE).forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int weight = Integer.valueOf(request.getParameter("weight")).intValue();
		int id = Integer.valueOf(request.getParameter("id")).intValue();
		EffortDao effortDao = new EffortDao();
		try {
			effortDao.changeweight(weight, id);
			request.getRequestDispatcher(EcHelper.MEIN_PAGE).forward(request, response);
			return;
		} catch (SQLException e) {
			e.printStackTrace();
			request.getRequestDispatcher(EcHelper.EFFORT_PAGE).forward(request, response);
			return;
		}
	}

}
