package dietsite;

import javax.servlet.http.HttpSession;

public class EcHelper {
//ログイン
static final String LOGIN_PAGE = "/WEB-INF/jsp/index.jsp";
static final String NEWUSER_PAGE = "/WEB-INF/jsp/newuser.jsp";
static final String MEIN_PAGE = "/WEB-INF/jsp/mainpage.jsp";
static final String CALORIE_PAGE = "/WEB-INF/jsp/eatstyle.jsp";
static final String EFFORT_PAGE = "/WEB-INF/jsp/effort.jsp";
static final String MENU_PAGE = "/WEB-INF/jsp/menu.jsp";
static final String WORKOUT_PAGE = "/WEB-INF/jsp/workout.jsp";


public static Object cutSessionAttribute(HttpSession session, String str) {
	Object test = session.getAttribute(str);
	session.removeAttribute(str);

	return test;
}

}

