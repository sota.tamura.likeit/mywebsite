package dietsite;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDAO;

/**
 * Servlet implementation class indexServlet
 */
@WebServlet("/indexServlet")
public class indexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher(EcHelper.LOGIN_PAGE).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String userid = request.getParameter("userid");
		String pass = request.getParameter("password");

		try {
			UserDAO UserDao = new UserDAO();
			UserBeans user = UserDao.findByLoginInfo(userid, pass);
			if (user == null) {
				request.setAttribute("errMsg", "ログインに失敗しました。");

				request.getRequestDispatcher(EcHelper.LOGIN_PAGE).forward(request, response);
				return;
			}else {
				HttpSession session = request.getSession();
				session.setAttribute("userInfo", user);
				request.getRequestDispatcher(EcHelper.MEIN_PAGE).forward(request, response);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
