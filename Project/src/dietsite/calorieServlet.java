package dietsite;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.CalorieDao;

/**
 * Servlet implementation class calorieServc
 */
@WebServlet("/calorieServlet")
public class calorieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("indexServlet");
			return;
		}
		CalorieDao calorieDao = new CalorieDao();
		int id = Integer.valueOf(request.getParameter("id")).intValue();
		try {
			UserBeans calorie = calorieDao.findcalorieuser(id);
			request.setAttribute("calorieInfo", calorie);
			request.getRequestDispatcher(EcHelper.CALORIE_PAGE).forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int breakfast = Integer.valueOf(request.getParameter("breakfast")).intValue();
		int lunch = Integer.valueOf(request.getParameter("lunch")).intValue();
		int dinner = Integer.valueOf(request.getParameter("dinner")).intValue();
		int id = Integer.valueOf(request.getParameter("id")).intValue();
		int sex = Integer.valueOf(request.getParameter("sex")).intValue();
		int rengeid = Integer.valueOf(request.getParameter("rengeid")).intValue();
		CalorieDao calorie = new CalorieDao();
		try {
			UserBeans already = calorie.caloriealready(id);
			if (already == null) {
				calorie.newcalorieStyle(id, breakfast, lunch, dinner, rengeid, sex);
			} else {
				calorie.calorieStyle(id, breakfast, lunch, dinner, rengeid, sex);
			}
			request.getRequestDispatcher(EcHelper.MEIN_PAGE).forward(request, response);
			return;

		} catch (SQLException e1) {
			e1.printStackTrace();
		}

	}

}
