package beans;

import java.io.Serializable;
import java.util.Date;

public class UserBeans implements Serializable {
	private int id;
	private String userid;
	private int sex;
	private double bmi;
	private Date createdate;
	private int weight;
	private int bminumber;
	private String password;
	private int height;
	private int old;
	private int firstweight;
	private int bmiId;
	private int rengeid;
	public UserBeans() {

	}
	public UserBeans(int id, String userid, double bmi, Date createdate, int weight, int bminumber, String password,
			int height, int old, int firstweight) {
		super();
		this.id = id;
		this.userid = userid;
		this.bmi = bmi;
		this.createdate = createdate;
		this.weight = weight;
		this.bminumber = bminumber;
		this.password = password;
		this.height = height;
		this.old = old;
		this.firstweight = firstweight;
	}
	public UserBeans(int id, String userid,String password) {
		this.id = id;
		this.userid = userid;
		this.password = password;
	}

	public UserBeans(int id, int sex,int bmiId, int rengeid) {
		this.id = id;
		this.sex=sex;
		this.bmiId = bmiId;
		this.rengeid = rengeid;
	}

	public UserBeans(int id, int rengeid,int sex) {
		this.id = id;
		this.rengeid = rengeid;
		this.sex=sex;
	}
	public UserBeans(int id) {
		this.id = id;
	}

	public UserBeans(String userid) {
		super();
		this.userid = userid;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}

	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public double getBmi() {
		return bmi;
	}
	public void setBmi(double bmi) {
		this.bmi = bmi;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public int getBminumber() {
		return bminumber;
	}
	public void setBminumber(int bminumber) {
		this.bminumber = bminumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getOld() {
		return old;
	}
	public void setOld(int old) {
		this.old = old;
	}
	public int getFirstweight() {
		return firstweight;
	}
	public void setFirstweight(int firstweight) {
		this.firstweight = firstweight;
	}
	public int getBmiId() {
		return bmiId;
	}
	public void setBmiId(int bmiId) {
		this.bmiId = bmiId;
	}
	public int getRengeid() {
		return rengeid;
	}
	public void setRengeid(int rengeid) {
		this.rengeid = rengeid;
	}

}
