package beans;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

public class EffortBeans implements Serializable {
	public EffortBeans() {
		super();
	}

	private int id;
	private int weight;
	private int first_weigh;
	private String style;
	private String calorie_style;
	private Time workouttime;
	private int breakfast;
	private int lunch;
	private int dinner;
	private int total;
	private int standerd;
	private double bmi;
	private Date createdate;

	public EffortBeans(int id, int weight,double bmi, int first_weigh, String style, String calorie_style, Time workouttime) {
		super();
		this.id = id;
		this.weight = weight;
		this.bmi= bmi;
		this.first_weigh = first_weigh;
		this.style = style;
		this.calorie_style = calorie_style;
		this.workouttime = workouttime;
	}

	public EffortBeans(int breakfast, int lunch, int dinner, int total, int standerd_calorie, Date createdate) {
		super();
		this.breakfast = breakfast;
		this.lunch = lunch;
		this.dinner = dinner;
		this.total = total;
		this.standerd = standerd_calorie;
		this.createdate = createdate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getFirst_weigh() {
		return first_weigh;
	}

	public void setFirst_weigh(int first_weigh) {
		this.first_weigh = first_weigh;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getCalorie_style() {
		return calorie_style;
	}

	public void setCalorie_style(String calorie_style) {
		this.calorie_style = calorie_style;
	}

	public Time getWorkouttime() {
		return workouttime;
	}

	public void setWorkouttime(Time workouttime) {
		this.workouttime = workouttime;
	}

	public int getBreakfast() {
		return breakfast;
	}

	public void setBreakfast(int breakfast) {
		this.breakfast = breakfast;
	}

	public int getLunch() {
		return lunch;
	}

	public void setLunch(int lunch) {
		this.lunch = lunch;
	}

	public int getDinner() {
		return dinner;
	}

	public void setDinner(int dinner) {
		this.dinner = dinner;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getStanderd() {
		return standerd;
	}

	public void setStanderd(int standerd) {
		this.standerd = standerd;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public double getBmi() {
		return bmi;
	}

	public void setBmi(double bmi) {
		this.bmi = bmi;
	}

}
