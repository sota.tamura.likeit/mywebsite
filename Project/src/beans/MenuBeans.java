package beans;
import java.io.Serializable;

public class MenuBeans implements Serializable{
	private int menu_id;
	private int bmi_id;
	private String workout_menu;
	private String eat_menu;
	private String style_explanation;
	private String workout_style;
	private String recommended_workout;
	private String recommended_eat_menu;
	private String style;



	public MenuBeans( String workout_menu, String eat_menu, String style_explanation,
			String workout_style, String recommended_workout, String recommended_eat_menu, String style) {
		super();
		this.workout_menu = workout_menu;
		this.eat_menu = eat_menu;
		this.style_explanation = style_explanation;
		this.workout_style = workout_style;
		this.recommended_workout = recommended_workout;
		this.recommended_eat_menu = recommended_eat_menu;
		this.style = style;
	}





	public int getMenu_id() {
		return menu_id;
	}
	public void setMenu_id(int menu_id) {
		this.menu_id = menu_id;
	}
	public int getBmi_id() {
		return bmi_id;
	}
	public void setBmi_id(int bmi_id) {
		this.bmi_id = bmi_id;
	}
	public String getWorkout_menu() {
		return workout_menu;
	}
	public void setWorkout_menu(String workout_menu) {
		this.workout_menu = workout_menu;
	}
	public String getEat_menu() {
		return eat_menu;
	}
	public void setEat_menu(String eat_menu) {
		this.eat_menu = eat_menu;
	}
	public String getStyle_explanation() {
		return style_explanation;
	}
	public void setStyle_explanation(String style_explanation) {
		this.style_explanation = style_explanation;
	}
	public String getWorkout_style() {
		return workout_style;
	}
	public void setWorkout_style(String workout_style) {
		this.workout_style = workout_style;
	}
	public String getRecommended_workout() {
		return recommended_workout;
	}
	public void setRecommended_workout(String recommended_workout) {
		this.recommended_workout = recommended_workout;
	}
	public String getRecommended_eat_menu() {
		return recommended_eat_menu;
	}
	public void setRecommended_eat_menu(String recommended_eat_menu) {
		this.recommended_eat_menu = recommended_eat_menu;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}


}
