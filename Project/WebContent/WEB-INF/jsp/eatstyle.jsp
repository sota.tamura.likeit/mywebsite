<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>calorie</title>
<link rel="stylesheet" href="css/eatstyle.css">
</head>
<body>
	<header>
		<div class="container">
			<div class="header-left">
				<a href="mainpageServlet">right zapp</a>
			</div>
			<div class="header-right">
				<a href="loguoutServlet">ログアウト </a> <a href="newuserServlet">
					新規登録</a>
			</div>
		</div>
	</header>
	<div class="btn-wrapper">
		<div class="btn-possion">
			<form action="calorieServlet" method="POST" class="login">
				<h1>BREAKFAST</h1>
				<input type="number" class="calorie" placeholder="朝のカロリー"
					name="breakfast" maxlength="5" required>
				<h1>LUNCH</h1>
				<input type="number" class="calorie" placeholder="昼のカロリー"
					name="lunch" maxlength="5" required>
				<h1>DINNER</h1>
				<input type="number" class="calorie" placeholder="夜のカロリー"
					name="dinner" maxlength="5" required> <input type="hidden"
					name="id" value="${calorieInfo.id}"> <input type="hidden"
					name="rengeid" value="${calorieInfo.rengeid}"> <input
					type="hidden" name="sex" value="${calorieInfo.sex}"> <input
					type="submit" class="button" value="加算" />

			</form>
			<div class="under">
				<button type="button" onclick="history.back()" class="back">戻る</button>
			</div>
		</div>
	</div>

</body>
</html>