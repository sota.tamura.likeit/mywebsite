<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>mainpage</title>
<link rel="stylesheet" href="css/mainpage.css">
</head>
<body>
	<header>
		<div class="container">
			<div class="header-left">
				<a href="indexServlet">right zapp</a>
			</div>
			<div class="header-right">
				<a href="loguoutServlet">ログアウト </a> <a href="newuserServlet">
					新規登録</a>
			</div>
		</div>
	</header>
	<div class="btn-wrapper">
		<h1>あなたをコミットさせたい</h1>
		<p>right zappでは、結果がわかるから続けられるをコンセプトにあなたをサポートします。</p>

		<div class="flex-list">
			<a href="calorieServlet?id=${userInfo.id}" class="calorie">CALORIE
			</a> <a href="workoutServlet?id=${userInfo.id}" class="workout">WORK
				OUT</a> <a href="menuServlet?id=${userInfo.id}" class="menu">MENU</a> <a
				href="effortServlet?id=${userInfo.id}" class="effort">EFFORT</a>
		</div>
	</div>
</body>
</html>