<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>effort</title>
<link rel="stylesheet" href="css/effort.css">
</head>
<body>
	<header>
		<div class="container">
			<div class="header-left">
				<a href="indexServlet">right zapp</a>
			</div>
			<div class="header-right">
				<a href="loguoutServlet">ログアウト </a> <a href="newuserServlet">
					新規登録</a>
			</div>
		</div>
	</header>
	<div class="btn-wrapper">
		<h1>EFFORT</h1>
	</div>
	<div class="main-wrapper">
		<ul class="flex-list">

			<li class="li1">CHANGE WEIGHT<br> <span>${effort.first_weigh}→${effort.weight}</span></li>
			<li class="li2">WORKOUT TIME<br> <span>${effort.workouttime}</span></li>
			<li class="li3">BMI<br> <span>${effort.bmi}</span></li>
			<li class="li4">BODY<br> <span>${effort.style}</span></li>
		</ul>
		<div class="card-content">
			<table border="1" class="table">

				<thead>
					<tr>
						<th class="center">DATE</th>
						<th class="center">BREAKFAST</th>
						<th class="center">LUNCH</th>
						<th class="center">DINNER</th>
						<th class="center">TOTAL</th>
						<th class="center">STANDERD CALORIE</th>
						<th class="center">CALORIE STYLE</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="calorie" items="${calorie}">
						<tr>
							<td class="center">${calorie.createdate}</td>
							<td class="center">${calorie.breakfast}</td>
							<td class="center">${calorie.lunch}</td>
							<td class="center">${calorie.dinner}</td>
							<td class="center">${calorie.total}</td>
							<td class="center">${calorie.standerd}</td>
							<td class="center">${calorie.calorie_style}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>


		</div>
		<form action="effortServlet" method="POST">
			<div class="weight">
				<h1>NOW WEIGHT</h1>
				<input type="number" class="calorie" placeholder="変わった体重"
					name="weight" maxlength="3" max="150"><input type="hidden"
					name="id" value="${effort.id}"><input type="submit"
					class="button" name="Login" value="change" />
			</div>


		</form>
		<div class="under">
			<button type="button" onclick="history.back()" class="back">戻る</button>
		</div>
	</div>



</body>
</html>