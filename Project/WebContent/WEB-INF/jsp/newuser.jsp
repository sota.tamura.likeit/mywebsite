<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>newuser</title>
<link rel="stylesheet" href="css/newuser.css">
</head>
<body>
	<header>
		<div class="container">
			<div class="header-left">
				<a href="mainpageServlet">right zapp</a>
			</div>
		</div>
	</header>
	<div class="error">${errMsg}</div>
	<div class="btn-wrapper">
		<form action="newuserServlet" method="POST">
			<div class="btn">
				<h1>ユーザーID</h1>
				<input type="text" class="inter" placeholder="user id" name="userid"
					maxlength="8" required>
			</div>
			<div class="btn">
				<h1>パスワード</h1>
				<input type="password" class="inter" placeholder="パスワード" name="pass"
					maxlength="8" required> <input type="password"
					class="inter" placeholder="再パスワード確認" name="repass" required>
			</div>
			<div class="btn">
				<h1>年齢</h1>
				<input type="number" class="calorie" name="old" placeholder="歳"
					maxlength="3" max="100" required>
			</div>
			<div class="btn">
				<h1>体重</h1>
				<input type="number" class="calorie" placeholder="kg" name="weight"
					maxlength="3" max="150" required>
			</div>
			<div class="btn">
				<h1>身長</h1>
				<input type="number" class="calorie" placeholder="cm" name="height"
					maxlength="3" max="200" required>
			</div>
			<div class="btn">
				<h1>性別</h1>
				<input type="radio" class="radio" name="sex" value="1" required>
				<p>男性</p>
				<input type="radio" class="radio" name="sex" value="2" required>
				<p>女性</p>
			</div>
			<input type="submit" class="button" value="登録" />
		</form>
		<div class="under">
			<button type="button" onclick="history.back()" class="back">戻る</button>
		</div>
	</div>
</body>
</html>