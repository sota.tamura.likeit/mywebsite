<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>index</title>
<link rel="stylesheet" href="css/index.css">
</head>
<body>
	<header>
		<div class="container">
			<div class="header-left">
				<a href="indexServlet">right zapp</a>
			</div>
			<div class="header-right">
				<a href="newuserServlet">新規登録</a>
			</div>
		</div>
	</header>
	<div class="btn-wrapper">
		<h1>RIGHT ZAPP</h1>
		<c:if test="${errMsg != null}">
			<p class="red-text center-align">${errMsg}</p>
		<br>
		</c:if>


		<form action="indexServlet" method="POST">
			<input type="text" class="login-userId" placeholder="Uer id(８文字以内)"
				name="userid" maxlength="8"> <input type="password"
				placeholder="password(８文字以内)" class="login-password" name="password"
				maxlength="8"> <input type="submit" class="button"
				name="Login" value="Login" />
			<div class="new-user">
				<a href="newuserServlet" class="btn signup">新規登録はこちら</a>
			</div>
		</form>
	</div>


</body>
</html>