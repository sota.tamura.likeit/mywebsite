<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>workout</title>
<link rel="stylesheet" href="css/workout.css">
</head>
<body>
	<header>
		<div class="container">
			<div class="header-left">
				<a href="indexServlet">right zapp</a>
			</div>
			<div class="header-right">
				<a href="loguoutServlet">ログアウト </a> <a href="newuserServlet">
					新規登録</a>
			</div>
		</div>
	</header>
	<div class="login-wrapp">
		<div class="workout">
			WORK <span style="color: #ffffff">OUT</span>
		</div>
		<div class="timer" id="timer">
            <h1>00:00</h1>
        </div>
		<div class="btn-wrapper">
			<input type="button" id="start" class="start" value="START" /><input
				type="button" id="reset" class="finish" value="RESET" /><input
				type="button" id="stop" class="start" value="STOP" />

			<form action="workoutServlet" method="POST">

				<input type="time" name="time" step="1" class="time" min="00:00:01"
					max="02:00:00" required> <input type="hidden" name="id"
					value="${workout}"> <input type="submit" id="reset" class="submit"
					value="送信" />
			</form>
		</div>
	</div>
</body>

<script type="text/javascript">
	(function() {
		'use strict';

		var timer = document.getElementById('timer');
		var start = document.getElementById('start');
		var stop = document.getElementById('stop');
		var reset = document.getElementById('reset');

		var startTime;

		var elapsedTime = 0;

		var timerId;

		var timeToadd = 0;

		function updateTimetText() {

			var m = Math.floor(elapsedTime / 60000);

			var s = Math.floor(elapsedTime % 60000 / 1000);

			m = ('0' + m).slice(-2);
			s = ('0' + s).slice(-2);

			timer.textContent = m + ':' + s;

		}

		function countUp() {

			timerId = setTimeout(function() {

				elapsedTime = Date.now() - startTime + timeToadd;
				updateTimetText()

				countUp();

			}, 10);
		}

		start.addEventListener('click', function() {

			startTime = Date.now();

			countUp();
		});

		stop.addEventListener('click', function() {

			clearTimeout(timerId);

			timeToadd += Date.now() - startTime;
		});

		reset.addEventListener('click', function() {
			elapsedTime = 0;

			timeToadd = 0;

			updateTimetText();

		});
	})();
</script>
</html>