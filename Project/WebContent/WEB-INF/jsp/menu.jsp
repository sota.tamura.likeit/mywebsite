<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>menu</title>
<link rel="stylesheet" href="css/menu.css">
</head>
<body>
	<header>
		<div class="container">
			<div class="header-left">
				<a href="indexServlet">right zapp</a>
			</div>
			<div class="header-right">
				<a href="loguoutServlet">ログアウト </a> <a href="newuserServlet">
					新規登録</a>
			</div>
		</div>
	</header>
	<div class="btn-wrapper">
		<h1>YOUR STYLE</h1>
		<table border="1" class="table">
			<tr>
				<th>体型説明</th>
			</tr>
			<tr>
				<td>${menu.style_explanation}</td>
			</tr>
		</table>
		<h1>WORKOUT STYLE</h1>
		<table border="1" class="table">
			<tr>
				<th>おすすめ運動</th>
				<th>運動方法</th>
				<th>おすすめ運動種類</th>
			</tr>
			<tr>
				<td>${menu.workout_menu}</td>
				<td>${menu.workout_style}</td>
				<td>${menu.recommended_workout}</td>
			</tr>
		</table>
		<h1>EAT STYLE</h1>
		<table border="1" class="table">
			<tr>
				<th>食事方法</th>
				<th>おすすめご飯</th>
			</tr>
			<tr>
				<td>${menu.eat_menu}</td>
				<td>${menu.recommended_eat_menu}</td>
			</tr>
		</table>
		<div class="under">
			<button type="button" onclick="history.back()" class="back">戻る</button>
		</div>
	</div>

</body>
</html>