<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>newuser</title>
<link rel="stylesheet" href="newuser.css">
</head>
<body>
	<header>
		<div class="container">
			<div class="header-left">
				<a href="mainpageServlet.java">right zapp</a>
			</div>
		</div>
	</header>
	<div class="btn-wrapper">
		<form action="newuserServlet.java" method="POST" class="login">
			<div class="btn">
				<h1>ユーザーID</h1>
				<input type="text" class="inter" placeholder="user id" name="userid">
			</div>
			<div class="btn">
				<h1>パスワード</h1>
				<input type="password" class="inter" placeholder="パスワード" name="pass">
				<input type="password" class="inter" placeholder="再パスワード確認"
					name="repass">
			</div>
			<div class="btn">
				<h1>年齢</h1>
				<input type="number" class="calorie" name="old">
			</div>
			<div class="btn">
				<h1>体重</h1>
				<input type="number" class="calorie" placeholder="60" name="weight">
			</div>
			<div class="btn">
				<h1>身長</h1>
				<input type="number" class="calorie" placeholder="170" name="height">
			</div>
			<div class="btn">
				<h1>性別</h1>
				<input type="radio" class="radio" name="男" >
				<p>男</p>
				<input type="radio" class="radio" name="女">
				<p>女</p>
			</div>

			<input type="submit" class="button" value="登録" />

		</form>
		<div class="under">
			<button type="button" onclick="history.back()" class="back">戻る</button>
		</div>
	</div>
</body>
</html>